SUMMARY = "QTI MACsec opensource package groups"
LICENSE = "BSD-3-Clause"
PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PROVIDES = "${PACKAGES}"

PACKAGES = "packagegroup-qti-macsec"

RDEPENDS_packagegroup-qti-macsec  = " \
        qca-macsec \
        qca-macsec-shell \
        "
