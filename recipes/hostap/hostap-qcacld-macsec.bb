inherit autotools linux-kernel-base pkgconfig

DESCRIPTION = "Support MACsec Hostap"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=3775480a712fc46a69647678acb234cb"

PR = "r5"

DEPENDS += "openssl wpa-supplicant-8-lib qca-macsec-shell"

FILESPATH =+ "${WORKSPACE}:"
SRC_URI = "file://external/wpa_supplicant_8/"
SRC_URI += "file://hostapd_defconfig-qcacld"

RPROVIDES_${PN}+="libfal.so"

S = "${WORKDIR}/external/wpa_supplicant_8"

HOSTAPD_CONFIG = "${S}/hostapd/.config"

do_configure() {
	install -m 0644 ${WORKDIR}/hostapd_defconfig-qcacld ${HOSTAPD_CONFIG}
	echo "CONFIG_DRIVER_MACSEC_QCA=y" >> ${HOSTAPD_CONFIG}
	echo "CONFIG_MACSEC=y" >> ${HOSTAPD_CONFIG}
	echo "CFLAGS += -I${STAGING_INCDIR}" >> ${HOSTAPD_CONFIG}
	echo "CFLAGS += -I${STAGING_INCDIR}/libnl3" >> ${HOSTAPD_CONFIG}
	echo "LDFLAGS += -L${STAGING_LIBDIR}" >> ${HOSTAPD_CONFIG}
	echo "LIBS += -lfal" >> ${HOSTAPD_CONFIG}
}

do_compile() {
	make -C ${S}/hostapd/
}

do_install() {
	install -d ${D}${sbindir}
    cd ${S}/hostapd
    install -m 0755 hostapd ${D}${sbindir}/hostapd-macsec
}
