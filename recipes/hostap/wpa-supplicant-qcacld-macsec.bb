inherit autotools linux-kernel-base

DESCRIPTION = "Support MACsec wpa_supplicant"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=3775480a712fc46a69647678acb234cb"

PR = "r5"

DEPENDS += "openssl glib-2.0 wpa-supplicant-8-lib qca-macsec-shell"

FILESPATH =+ "${WORKSPACE}:"
SRC_URI = "file://external/wpa_supplicant_8/"
SRC_URI += "file://wpa_supplicant_defconfig-qcacld"

RPROVIDES_${PN}+="libfal.so"

S = "${WORKDIR}/external/wpa_supplicant_8"
SUPPLICANT_CONFIG = "${S}/wpa_supplicant/.config"

do_configure() {
	install -m 0644 ${WORKDIR}/wpa_supplicant_defconfig-qcacld ${SUPPLICANT_CONFIG}
	echo "CONFIG_DRIVER_MACSEC_QCA=y" >> ${SUPPLICANT_CONFIG}
	echo "CFLAGS += -I${STAGING_INCDIR}" >> ${SUPPLICANT_CONFIG}
	echo "CFLAGS += -I${STAGING_INCDIR}/libnl3" >> ${SUPPLICANT_CONFIG}
	echo "LDFLAGS += -L${STAGING_LIBDIR}" >> ${SUPPLICANT_CONFIG}
	echo "LIBS += -lfal" >> ${SUPPLICANT_CONFIG}
}

do_compile() {
	make -C ${S}/wpa_supplicant/
}

do_install() {
	install -d ${D}${sbindir}
	cd ${S}/wpa_supplicant
	install -m 0755 wpa_supplicant ${D}${sbindir}/wpa_supplicant-macsec
}
