SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "NSS MACsec Driver shell"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=3775480a712fc46a69647678acb234cb"

inherit pkgconfig

DEPENDS += "glib-2.0"
FILESPATH =+ "${WORKSPACE}:"
FILESEXTRAPATHS_prepend := "${THISDIR}/files/:"
SRC_URI = "file://qcom-opensource/qca-macsec/"

S = "${WORKDIR}/qcom-opensource/qca-macsec"

PR = "r0"

INSANE_SKIP_${PN} += "already-stripped"
INSANE_SKIP_${PN}-dev += "dev-elf"

RPROVIDES_${PN}+="libfal.so"

CFLAGS +="-mfpu=neon -mfloat-abi=hard"
CFLAGS +=" -I${STAGING_INCDIR}"
CFLAGS +="-DUSE_GLIB `pkg-config --cflags glib-2.0`"
LDFLAGS +="--sysroot=${STAGING_DIR_TARGET}"
LDFLAGS +="`pkg-config --libs glib-2.0`"

QCA_MACSEC_CONFIG_OPTS+= "TOOL_PATH=${STAGING_BINDIR_TOOLCHAIN} \
						SYS_PATH=${STAGING_KERNEL_BUILDDIR} \
						TOOLPREFIX=${TARGET_PREFIX} \
						KVER=${KERNEL_VERSION} \
						ARCH='arm'"
do_compile() {
	${MAKE} -C ${S} ${QCA_MACSEC_CONFIG_OPTS} -f Makefile.shell
}

do_install() {
    install -d ${D}${libdir} ${D}${sbindir} ${D}${includedir}
	install -m 0755 libfal.so ${D}${libdir}
	install -m 0755 macsec_shell ${D}${sbindir}
	install -m 0644 include/nss_macsec_types.h ${D}${includedir}
	install -m 0644 include/nss_macsec_secy.h ${D}${includedir}
	install -m 0644 include/nss_macsec_secy_rx.h ${D}${includedir}
	install -m 0644 include/nss_macsec_secy_tx.h ${D}${includedir}
}
