SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "NSS MACsec Driver"
LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=f3b90e78ea0cffb20bf5cca7947a896d"

inherit module qperf

FILESPATH =+ "${WORKSPACE}:"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "file://qcom-opensource/qca-macsec/"
DEPENDS = "virtual/kernel"

S = "${WORKDIR}/qcom-opensource/qca-macsec"

PR = "r0"

PACKAGES += "kernel-module-qca-macsec"

EXTRA_OEMAKE += "TOOL_PATH='${STAGING_BINDIR_TOOLCHAIN}' \
		SYS_PATH='${STAGING_KERNEL_BUILDDIR}' \
		TOOLPREFIX='arm-oe-linux-gnueabi-' \
		KVER='${KERNEL_VERSION}' \
		ARCH='arm' \
		"
